---
layout: post
title: "Compilação de programas"
date: 2021-07-16 18:25:53
img: cover-compilarProgramas.png #add image name.png|jpg|etc
description: ''
fig-caption:
tags: [linux, tutoriais]
author: 'Miguel Rosa'
---


# Compilação de programas/software
Andava aqui a estudar alguns tópicos para a Certificação Linux quando me deparei com um tópico que, embora nos dias de hoje não seja "tão frequente", é sempre bom saber o procedimento quando o software apenas tem o código fonte disponível para compilar e instalar.

Para a compilção são necessários três comandos:

 * **./configure**
 * **make**
 * **make install**

O primeiro comando (**./configure**) é um script que irá configurar a compilação, isto é, faz uma pesquisa à procura de dependências, se existe algum compilador no sistema e se este compilador consegue criar executáveis e ainda procura outras exigências do próprio programa. Se algo de errado acontecer com esta pesquisa, o script automaticamente pára e mostra uma mensagem de erro. Estas mensagens são simples e directas, se uma pessoa perceber um pouco de inglês facilmente identifica o erro.

O segundo comando (**make**) vai juntar o (ou os vários) ficheiro(s) “*makefile*” que é (são) criado(s) pelo *configure*. Nestes ficheiro estão todas as configurações que vão ser aplicadas no momento da compilação, isto é, após iniciar o “make”, a compilação começa, no configure nada está sendo compilado… ainda! Se acontecer algum erro (caso raro) temos várias opções: 
* Ou mandar um mail ao pessoal que desenvolveu o programa;
* Ou então poderá ser a falta de uma biblioteca que não foi verificada quando fez o ./configure.

O processo de compilação poderá ser demorado (depende da aplicação).

Por fim, o terceiro comando (**make install**) é o mesmo que o “*make*” mas com outra finalidade. É com este comando que vamos instalar os executáveis já compilados e outros ficheiros necessários ao programa. Como este comando instala um programa, é necessário que seja executado como root, pois o programa copia pastas que o utilizador “normal” normalmente não tem acesso de escrita.

Existem, por vezes, algumas excepções, tais como:
 * Existem programas que não trazem o tal script do “*configure*”, estes ficheiros normalmente são drivers. Quando tal acontecer, pode começar o processo de compilação pelo segundo comando.
 * Se gosta de ter o seu sistema organizado, pode utilizar um prefixo comum. Para utilizar este prefixo, adicione **–-prefix=** no ./configure. Geralmente usa-se o *–-prefix=/usr*, ficando o comando: **./configure –-prefix=/usr**.


<br>

## Considerações finais
Estes são os comandos "básicos" para se proceder à compilação e instalação de software em Linux, contudo, recomendo vivamente a leitura do ficheiro *README* ou *INSTALL* que acompanha o ficheiro compactado para mais detalhes de compilação e/ou instalação. Para saber mais sobre o ./configure, digite *./configure –-help*.

Espero que este pequeno post tenha sido útil.
Vemo-nos no próximo post.
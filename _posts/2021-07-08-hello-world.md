---
layout: post
title: "Hello World"
date: 2021-07-08 14:20:54
img: cover-helloWorld.png #add image name.png|jpg|etc
description: ''
fig-caption:
tags: [blog, hello world]
author: 'Miguel Rosa'
---



# Hello (my) World!
Depois de alguns anos parado na blogosfera e de muita ponderação (e algum apoio), eis que decidi voltar a escrever!
O presente blog está muito longe dos sistemas utilizados anteriormente (blog no extinto *myopera* e *wordpress*), uma vez que deixei de utilizar sistemas complexos recurrendo a base de dados, configurações super-hiper-mega complicadas.

Assim, o presente cantinho conta com a minha visão/opinião sobre os temas apresentados, partilhando também vários guias e/ou tutoriais, artigos esses que podem (ou não) surgir em formato vídeo no meu canal do youtube (basta clicar no icon no canto inferior esquerdo).


<br>

### Porquê ***LazyTux***?
O título é apenas um trocadilho para os tempos que estamos a viver no presente. Muitas pessoas, nas correrias das suas vidas, tornaram-se algo preguiçosas e pretendem a informação rápida, curta, concisa, directo ao ponto! Perderam aquele tempo de explorar, ler, fazer e errar. No outro lado da moeda, para fazer chegar essa informação a essas pessoas, existem outras que, tal como eu, gostam de explorar, fazer, errar e transmitir conhecimento sem que seja cobrado nada! 

No Linux, com o uso massivo de distribuições *User Friendly* muitos dos processos já estão automatizados, no entanto, há uma outra infinidade de conhecimento que se pode passar, tornando assim o conhecimento das tarefas mais técnicas em algo que um comum utilizador de computador pode fazer.


<br>

### Pronto ok, e que mais?
Sou um curioso e apaixonado por tecnologia desde muito cedo, fazendo com que esse fosse o meu percurso profissional, a área da Informática. Sou utilizador, adepto do FOSS (**F**ree **O**pen **S**ource **S**oftware) quer a nível de Sistema Operativo, quer a nível de ferramentas que uso no dia a dia. No que se refere a equipamento, irei falar num próximo post.

Após esta breve apresentação, espero vê-lo mais vezes neste canto virtual.